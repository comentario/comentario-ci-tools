$(document).ready(function () {

    const cc = $('comentario-comments')[0];

    // Bind the "Run Comentario" button to the "main" function of the comments tag
    $('button[data-run-comentario]').click(()  => cc.main());

    // Bind the "Run with SSO" button to initialisation with non-interactive SSO afterwards
    $('button[data-run-comentario-with-sso]').click(() => {
        cc.main().then(() => cc.nonInteractiveSsoLogin());
    });

    // Embed Comentario with the "Insert Comentario" button
    let idxInstance = 0;
    $('button[data-insert-comentario]').click(function () {
        // Add a script tag to the <head>, if it doesn't exist yet
        if (!$('script[id="comentario-script"]').length) {
            const scr = document.createElement('script');
            scr.id = 'comentario-script';
            scr.defer = true;
            scr.src = '{{ .Site.Params.comentarioURL }}/comentario.js';
            document.head.appendChild(scr);
        }

        // Insert the comments tag
        $(this).after('<comentario-comments id="com-' + (++idxInstance) + '"></comentario-comments>');
    });
});
