---
title: Unregistered domain
menu:
    sidebar:
        weight: 999
        title: Unregistered domain
        params:
          url: http://127.0.0.1:8000/unregistered/
---

This page should fail loading comments because the domain `127.0.0.1` is not registered in Comentario.
