---
title: Dark mode
menu:
    sidebar:
        weight: 150
        title: Dark mode
rootAttrs:
    # This activates the dark theme for Bootstrap styles
    data-bs-theme: dark
comentarioTagAttrs:
    # This makes Comentario use the dark theme
    theme: dark
---

This page is rendered in the dark theme by default.
