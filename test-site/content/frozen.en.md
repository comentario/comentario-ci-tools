---
title: Frozen
menu:
    sidebar:
        weight: 110
        title: Read-only domain
        params:
          url: http://lacolhost.com:8000/frozen/
---

Comment thread on this page is locked because the entire domain is made read-only.
