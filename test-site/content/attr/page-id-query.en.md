---
title: 'Attribute: page-id, with a query'
comentarioTagAttrs:
    page-id: /another-page/?id=123
aliases:
  - /another-page
---

This page overrides the default path from `{{< page-path >}}` to `/another-page/?id=123`.
