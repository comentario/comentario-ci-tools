---
title: 'Attribute: page-id'
menu:
    sidebar:
        weight: 260
comentarioTagAttrs:
    page-id: /different-page/123
aliases:
  - /different-page/123
---

This page overrides the default path from `{{< page-path >}}` to `/different-page/123`.
