---
title: 'Attribute: live-update=false'
menu:
    sidebar:
        weight: 230
comentarioTagAttrs:
    live-update: false
---

Comments on this page won't be updated live via WebSockets when changed elsewhere.
