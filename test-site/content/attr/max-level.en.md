---
title: 'Attribute: max-level=2'
menu:
    sidebar:
        weight: 240
comentarioTagAttrs:
    max-level: 2
---

Comment threads on this page are limited to **2** levels depth.
