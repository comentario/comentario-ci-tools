---
title: 'Attribute: auto-non-interactive-sso=true'
menu:
    sidebar:
        weight: 205
comentarioTagAttrs:
    auto-non-interactive-sso: true
---

The user will be automatically logged is via SSO as soon as this page is opened (*unless already logged in*).
