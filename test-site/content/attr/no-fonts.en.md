---
title: 'Attribute: no-fonts=true'
menu:
    sidebar:
        weight: 250
comentarioTagAttrs:
    no-fonts: true
---

Comentario will not use its own font on this page. All comments will be displayed in the default page font.
