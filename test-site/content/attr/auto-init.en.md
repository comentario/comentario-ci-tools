---
title: 'Attribute: auto-init=false'
menu:
    sidebar:
        weight: 200
comentarioTagAttrs:
    auto-init: false
---

Comentario isn't automatically initialised on this page. Click the button below to load comments:

{{< comentario-run-button >}} {{< comentario-run-with-sso-button >}}

---
