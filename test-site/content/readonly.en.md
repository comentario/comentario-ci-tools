---
title: Read-only
menu:
    sidebar:
        weight: 100
        title: Read-only page
---

Comment thread on this page is locked because the page is made read-only.
