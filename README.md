# comentario-ci-tools

Tooling for Continuous Integrations.

# Setting up a Kubernetes cluster on GitLab

All repositories in the **comentario** group are using GitLab agent based Kubernetes config named `ys-comentario` (and located under `.gitlab/agents/`), added manually once.

The command to install a new agent in the cluster is provided when [registering the agent](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#register-the-agent-with-gitlab) from file.

# Running the test-site

1. Build a test-site image locally:
```bash
cd test-site
docker build -t test-site --build-arg BUILDER_TAG=registry.gitlab.com/comentario/comentario-ci-tools/builder:v12 .
```
2. Run the built image:
```bash
docker run --rm -t -p 8000:80 test-site
```
3. Optional: start the Comentario backend if you really want to see comments appear.
4. Point your browser at [localhost:8000](http://localhost:8000/)
